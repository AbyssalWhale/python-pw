import conftest
from Models.POM.HomePage import HomePage
from typing import Generator
import json
import os
from playwright.sync_api import expect
from playwright.sync_api import Playwright, APIRequestContext
from datetime import datetime
import pytest
import allure
import threading

test_run_config = None
test_run_content_folder = None
api_request_context = None
home_page = None


@allure.title("One Time SetUp")
@pytest.fixture(scope="session", autouse=True)
def one_time_set_up():
    with allure.step("Reading Configs"):
        _read_api_header()
    with allure.step("Creating test run folder"):
        lock = threading.Lock()
        lock.acquire()
        try:
            if conftest.test_run_content_folder is None:
                conftest.test_run_content_folder = get_project_root() + '\\TestResults\\' + datetime.now().strftime(
                    "%Y-%m-%d_%H-%M-%S")
                if not os.path.exists(conftest.test_run_content_folder):
                    os.makedirs(conftest.test_run_content_folder, exist_ok=True)
        finally:
            lock.release()


@allure.title("SetUp with Tear Down")
@pytest.fixture(scope="session")
def set_up(one_time_set_up, playwright: Playwright):
    with allure.step("Initializing browser"):
        browser = playwright.chromium.launch(headless=True)
    with allure.step("Initializing video record"):
        context = browser.new_context(
            record_video_dir=f"{conftest.test_run_content_folder}/",
            record_video_size={"width": 640, "height": 480}
        )

    with allure.step("Opening and verifying home page"):
        page = context.new_page()
        conftest.home_page = HomePage(page)

        expect(conftest.home_page.p_Page).to_have_title(conftest.home_page.page_title)
        expect(conftest.home_page.label_title_table).to_be_visible()
        expect(conftest.home_page.label_title_genres).to_be_visible()

    yield conftest.home_page

    with allure.step("Closing page and context"):
        conftest.home_page.p_Page.close()
        context.close()
    with allure.step("Attaching test artifacts"):
        print(f"Video Record: {page.video.path()}")
        allure.attach.file(page.video.path(), name="Video", attachment_type=allure.attachment_type.WEBM)


@pytest.fixture(scope="session")
def api_request_context(playwright: Playwright) -> Generator[APIRequestContext, None, None]:
    request_context = playwright.request.new_context(base_url="https://api.rawg.io/api/")
    conftest.api_request_context = request_context

    yield conftest.api_request_context
    request_context.dispose()


def _init_Home_Page(playwright: Playwright):
    browser = playwright.chromium.launch(headless=False)
    page = browser.new_page()
    home_page = HomePage(page)

    expect(home_page.p_Page).to_have_title(home_page.page_title)
    expect(home_page.label_title_table).to_be_visible()
    expect(home_page.label_title_genres).to_be_visible()

    home_page = home_page

    yield home_page


def _read_api_header():
    full_File_Path = f'{get_project_root()}//run-config.json'
    if os.path.exists(full_File_Path):
        with open(full_File_Path) as f:
            conftest.test_run_config = json.load(f)
    else:
        raise Exception(f"Could not find file with api headers. Path: {full_File_Path}")


def get_project_root():
    current_dir = os.path.abspath(os.path.dirname(__file__))
    while not os.path.isfile(os.path.join(current_dir, 'conftest.py')):
        parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
        if parent_dir == current_dir:
            raise Exception("Could not find project root directory.")
        current_dir = parent_dir
    return current_dir
