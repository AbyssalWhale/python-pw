#Win:
#build: docker build -t pytest-playwright-docker .
#PS: docker run -e API_KEY="" -v "${PWD}/TestResults:/app/TestResults" -i -t pytest-playwright-docker

#Base image with Python and dependencies
FROM python AS builder

ENV API_KEY="default"
ENV SET_CONFIG_SCRIPT_PATH="/app/set-config.sh"

VOLUME /app/TestResults

# Set the working directory in the container
WORKDIR /app

# Copy the test code and requirements.txt to the container
COPY . /app

# Install py and pip
RUN apt-get update
RUN apt install python-is-python3 -y 
RUN apt install python3-pip -y

# Install pytest
RUN pip install flake8 pytest-playwright 
RUN pip install pytest-reporter-html1 
RUN pip install pytest-xdist

#Install playwright
RUN python -m pip install playwright 
RUN playwright install-deps
RUN playwright install chromium
#RUN apt-get install -y nodejs
#RUN apt-get install -y npm
#RUN npx playwright install-deps

#Allure
RUN pip install allure-pytest allure-python-commons allure-python-commons

# Permissions
RUN chmod +x "/app/set-config.sh"

# Run pytest and generate the test result folder
CMD ["sh", "-c", "ls && ${SET_CONFIG_SCRIPT_PATH} ${API_KEY} && cat /app/run-config.json && pytest -m regression --junitxml=/app/TestResults/junit_results.xml --report=/app/TestResults/index.html --template=html1/index.html -n auto --alluredir=/app/TestResults/allure-result"]
#CMD pytest -m regression --junitxml=/app/TestResults/junit_results.xml --report=/app/TestResults/python_tests_result_report.html --template=html1/index.html -n auto --alluredir=/app/TestResults/Allure


