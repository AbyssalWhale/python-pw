#!/bin/bash

# Check if the number of arguments is correct
if [ $# -ne 1 ]; then
  echo "Usage: $0 value1"
  exit 1
fi

value1=$1
output_file="run-config.json"

# Check if the file exists
if [ -e "$output_file" ]; then
  # Update the existing file
  cat <<EOF > "$output_file"
{
  "key": "$value1"
}
EOF
else
  # Create a new file
  cat <<EOF > "$output_file"
{
  "key": "$value1"
}
EOF
fi
